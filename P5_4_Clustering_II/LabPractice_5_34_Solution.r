#################################################################################
##############       LabPractice 5.3_4  Clustering     ############################
##############     ----------- solution ---------    ############################
#################################################################################

library(ggplot2)
library(cclust)
library(dendextend)
library(stats)
library(factoextra)
library(NbClust)
library(MLTools)
library(cluster)

## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read.table("Aggregation.dat",header = FALSE, sep = "", stringsAsFactors = FALSE)
ggplot(fdata)+geom_point(aes(x=V1,y=V2))


## Hierarchical clustering -------------------------------------------------------------------------------------------------------
# Compute distances
dd <- dist(scale(fdata[,1:2]), method = "euclidean")
# hierarchical clustering
hc <- hclust(dd, method = "ward.D2")
plot(hc,hang = -1)
# Convert to dendrogram object
#dend <- as.dendrogram(hc)

#Cut the tree to a given number of clusters and obtain the associated cluster for each observation
cluster <-  cutree(hc, k = 4) #Use "h = " to cut by a given height 
table(cluster) #check number of observations assigned to each cluster, muestra los clusteres
#ggplot(fdata)+geom_point(aes(x=V1,y=V2,color=as.factor(cluster)))
#plot the dendrogram changing the colors according to the number of clusters
hc.col=color_branches(as.dendrogram(hc),k=4)# indico en funcion de lo que veo en el dendograma que quiero 4 grupos
plot(hc.col)
PlotClusters(NULL,cluster,fdata[,1:2])# no tengo informacion de centroides opr eso pongo NULL





## kmeans clustering -------------------------------------------------------------------------------------------------------
set.seed(101)
# Calculate clusters. "centers = " specifies the number of clusters to be obtained
km <- kmeans(fdata[,1:2], centers = 3)# indicamos que queremos K centros
# Plot the clusters and the centers
PlotClusters(km$centers,km$cluster,fdata[,1:2])# km$centers tiene la informacion de los centros y sus coordenadas
# puntos verdes que aparecen asignados a otro punto ya que realmente estan mas cerca del otro pero en relidad pertenecerian al otro


## Selecting the number of clusters -------------------------------------------------------
fviz_nbclust(fdata[,1:2], kmeans, method = "wss")+
  labs(subtitle = "Elbow method")# segun esto 4 quizas 6 apurando despues de 6 ya no muy plano se desagrega 
fviz_nbclust(fdata[,1:2], kmeans, method = "silhouette")+
  labs(subtitle = "Silhouette method")# compara como de agrupados estan y separados respecto a los demas, buscamos el maximo posible aqui 4
nb <- NbClust(fdata[,1:2], distance = "euclidean", min.nc = 2,
              max.nc = 9, method = "kmeans") # prueba muchas medidas y te entrega las conslusiones
par(mfrow=c(1,1)) #reset axis
fviz_nbclust(nb)
#Calculated selected number of clusters
km <- kmeans(fdata[,1:2], 4)
PlotClusters(km$centers,km$cluster,fdata[,1:2])

#silhouette plot for each point
sil <- silhouette(km$cluster, dd)
fviz_silhouette(sil)# silhouette para cada punto, observamos varios negativos mas cerca de otro cluster que sus compañeros de cluster





##neural gas - selection of k ---------------------------------------
library(cclust)
tunegrid <- seq(2,21,by = 1)
ssq <- matrix(ncol = length(tunegrid),nrow = 1)
silv <- matrix(ncol = length(tunegrid),nrow = 1) 
i <- 1
for(k in tunegrid){
  ng <- cclust(as.matrix(fdata[,1:2]),centers = k,method = "neuralgas")
  #Total sum of squares and silhouette
  ssq[i] <- sum(ng$withinss)
  sil <- silhouette(ng$cluster, dd)
  silv[i] <- mean(sil[,3])
  i <- i+1
}
par(mfrow=c(2,1))
plot(tunegrid,ssq,type="b")
plot(tunegrid,silv,type="b")# vemos que con 4 se podria reconstruir bien
par(mfrow=c(1,1))

#fit final model
ng <- cclust(as.matrix(fdata[,1:2]),centers = 4,method = "neuralgas")
PlotClusters(ng$centers,ng$cluster,fdata[,1:2])
#silhouette average
sil <- silhouette(ng$cluster, dd)
mean(sil[,3])


#Prbfn - Gaussian Mixture --------------------------------------------
library(mclust)
tunegrid <- seq(2,18,by = 1)
ssq <- matrix(ncol = length(tunegrid),nrow = 1)
silv <- matrix(ncol = length(tunegrid),nrow = 1) 
i <- 1
for(k in tunegrid){
  Sys.sleep(0.2)
  prbfn <- Mclust(fdata[,1:2],G=k,modelNames = "VVI")# mclust densidad
  #Total sum of squares and silhouette
  ssq[i] <- Reconstruction.Error(t(prbfn$parameters$mean),prbfn$classification,fdata[,1:2])
  sil <- silhouette(prbfn$classification, dd)
  silv[i] <- mean(sil[,3])
  i <- i+1
}
par(mfrow=c(2,1))
plot(tunegrid,ssq,type="b")
plot(tunegrid,silv,type="b")
par(mfrow=c(1,1))

#fit final model
prbfn <- Mclust(fdata[,1:2],G=7,modelNames = "VVI")
PlotClusters(t(prbfn$parameters$mean),prbfn$classification,fdata[,1:2])# puntos centro modelados con las gausianas
#silhouette average
sil <- silhouette(prbfn$classification, dd)
mean(sil[,3])





################# High dimensional data #############################
## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read.table("Dem_2011_2015_24h.dat",header = TRUE, sep = "", stringsAsFactors = FALSE)
matplot(t(as.matrix(fdata[1:5,3:26])),type="l")

##Hierarchical clustering
# Compute distances and hierarchical clustering
dd <- dist(scale(fdata[,3:26]), method = "euclidean")#matriz de distancias
hc <- hclust(dd, method = "ward.D2")
plot(hc,hang = -1)

#Cut the tree to a given number of clusters
cluster <-  cutree(hc, k = 3) #Use "h = " to cut by a given height 
table(cluster) #check number of observations assigned to each cluster
#plot the dendrogram changing the colors according to the number of clusters
hc.col=color_branches(as.dendrogram(hc),k=3)
plot(hc.col)

#Color curves according to cluster
matplot(t(as.matrix(fdata[,3:26])),type="l",col = cluster)
# Plot the cluster in Principal Component subspace
PlotClusters(NULL,cluster,fdata[,3:26])# aqui haciendo pca internamente permite visualizar la varianza de tantas variables

#Compare cluster and category
table(cluster,fdata$DAY)# vemos como esta poblado un cluster en funcion a una variable



##Kmeans
#Select optimum number of clusters
fviz_nbclust(fdata[,3:26], kmeans, method = "wss")+
  labs(subtitle = "Elbow method")
fviz_nbclust(fdata[,3:26], kmeans, method = "silhouette")+
  labs(subtitle = "Silhouette method")
nb <- NbClust(fdata[,3:26], distance = "euclidean", min.nc = 2,
              max.nc = 9, method = "kmeans")
fviz_nbclust(nb)
#Calculated selected number of clusters
km <- kmeans(fdata[,3:26], 3)
#Color curves according to cluster
matplot(t(as.matrix(fdata[,3:26])),type="l",col = km$cluster)
# Plot the cluster in Principal Component subspace and represent centers
PlotClusters(km$centers,km$cluster,fdata[,3:26])

#Compare cluster and category
table(km$cluster,fdata$DAY)



## neural gas - selection of k -----------------------------------------
tunegrid <- seq(2,20,by = 1)
ssq <- matrix(ncol = length(tunegrid),nrow = 1)
silv <- matrix(ncol = length(tunegrid),nrow = 1) 
i <- 1
for(k in tunegrid){
  ng <- cclust(as.matrix(fdata[,3:26]),centers = k,method = "neuralgas")
  #Total sum of squares and silhouette
  ssq[i] <- sum(ng$withinss)
  sil <- silhouette(ng$cluster, dd)
  silv[i] <- mean(sil[,3])
  i <- i+1
}
par(mfrow=c(2,1))
plot(tunegrid,ssq,type="b")
plot(tunegrid,silv,type="b")
par(mfrow=c(1,1))

#fit final model
ng <- cclust(as.matrix(fdata[,3:26]),centers = 3,method = "neuralgas")
PlotClusters(ng$centers,ng$cluster,fdata[,3:26])
#silhouette average
sil <- silhouette(ng$cluster, dd)
mean(sil[,3])



##prbfn - Gaussian Mixture --------------------------------------------------
tunegrid <- seq(2,15,by = 1)
ssq <- matrix(ncol = length(tunegrid),nrow = 1)
silv <- matrix(ncol = length(tunegrid),nrow = 1) 
i <- 1
for(k in tunegrid){
  Sys.sleep(0.2)
  prbfn <- Mclust(fdata[,3:26],G=k,modelNames = "VVI")
  #Total sum of squares and silhouette
  ssq[i] <- Reconstruction.Error(t(prbfn$parameters$mean),prbfn$classification,fdata[,3:26])
  sil <- silhouette(prbfn$classification, dd)
  silv[i] <- mean(sil[,3])
  i <- i+1
}
par(mfrow=c(2,1))
plot(tunegrid,ssq,type="b")
plot(tunegrid,silv,type="b")
par(mfrow=c(1,1))

prbfn <- Mclust(fdata[,3:26], G=3, modelNames = "VVI")
PlotClusters(t(prbfn$parameters$mean),prbfn$classification,fdata[,3:26])
#silhouette average
sil <- silhouette(prbfn$classification, dd)
mean(sil[,3])



