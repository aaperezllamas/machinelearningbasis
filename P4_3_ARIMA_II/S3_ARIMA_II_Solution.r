#################################################################################
##############           S3 ARIMA II Example         ############################
##############     ----------- solution ---------    ############################
#################################################################################

library(MLTools)
library(fpp2)
library(ggplot2)
library(readxl)
library(lmtest)  #contains coeftest function
library(tseries) #contains adf.test function

## Set working directory ---------------------------------------------------------------------------------------------


## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read_excel("ARIMA_II.xls")
# Convert to time series object
fdata_ts <- ts(fdata)
# index to select a time series
y <- fdata_ts[,2]

## Load custom functions ---------------------------------------------------------------
# source("ForecastingTools.R")


## Identification and fitting frocess -------------------------------------------------------------------------------------------------------
autoplot(y)

# Box-Cox transformation
Lambda <- BoxCox.lambda.plot(y,10)
# Lambda <- BoxCox.lambda(y) #other option
z <- BoxCox(y,Lambda)
autoplot(z)

# Differentiation: if the ACF decreases very slowly -> needs differenciation
ggtsdisplay(z,lag.max = 25)
# Alternative test
adf.test(y, alternative = "stationary")
ndiffs(y)
# If differencing is needed
Bz <- diff(z,differences = 1)

# ACF and PACF of the time series -> identify significant lags and order
ggtsdisplay(Bz,lag.max = 25)

# Fit model with estimated order
arima.fit <- Arima(y,
                   order=c(0,0,0), 
                   lambda = Lambda,
                   include.constant = TRUE)
summary(arima.fit) #summary of training errors and estimated coefficients
coeftest(arima.fit) #statistical significance of estimated coefficients
autoplot(arima.fit) #root plot
# Check residuals
CheckResiduals.ICAI(arima.fit, bins = 100)
# If residuals are not white noise, change order of ARMA

#######

#Check  forecast
autoplot(y, series = "Real")+
  forecast::autolayer(arima.fit$fitted, series = "Fitted")

#Perform future forecast
y_est <- forecast(arima.fit, h=12)
autoplot(y_est)


