#############################################################################
##############      Lab 4.6:   Transfer Function     ############################
##############     ----------- solution ---------    ############################
#################################################################################

library(MLTools)
library(fpp2)
library(ggplot2)
library(TSA)
library(lmtest)  #contains coeftest function
library(tseries) #contains adf.test function
library(Hmisc) # for computing lagged variables

## Set working directory ---------------------------------------------------------------------------------------------


## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read.table("TF1.dat",header = TRUE, sep = "")
# Convert to time series object
fdata_ts <- ts(fdata)
autoplot(fdata_ts, facets = TRUE)
# Create time series and scale values 
y <- ts(fdata$y)/10 # escalar para que no haya valores muy grandes
x <- ts(fdata$x)/1



## Identification and fitting process -------------------------------------------------------------------------------------------------------

#### Fit initial FT model with large s
# This arima function belongs to the TSA package
TF.fit <- arima(y,
                order=c(1,1,0),
                #seasonal = list(order=c(1,0,0),period=24),
                xtransf = x,
                transfer = list(c(0,9)), #List with (r,s) orders
                include.mean = TRUE,
                method="ML")
summary(TF.fit) # summary of training errors and estimated coefficients
coeftest(TF.fit) # statistical significance of estimated coefficients
# Check regression error to see the need of differentiation
TF.RegressionError.plot(y,x,TF.fit,lag.max = 100)# Decrece lentamente, indica que hay que identificar de manera regular(1,1,0)
#NOTE: If this regression error is not stationary in variance,boxcox should be applied to input and output series.
# despues de primera modificacion vemos como s=1 hay un retardo hasta
# que empieza a decrecerpey, pero como b=1 se ignora el primero r = 1 decrece lineal, b 1 hay uno que caso es 0
#ar 1 degenerado lo que hace es diferenciar
# Check numerator coefficients of explanatory variable
TF.Identification.plot(x,TF.fit)


#### Fit arima noise with selected
xlag = Lag(x,1)   # b
xlag[is.na(xlag)]=0
arima.fit <- arima(y,
                   order=c(2,1,1),
                   #seasonal = list(order=c(1,1,0),period=24),
                   xtransf = xlag,
                   transfer = list(c(1,0)), #List with (r,s) orders
                   include.mean = FALSE,
                   method="ML")
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
# Check residuals
CheckResiduals.ICAI(arima.fit)
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 50)


### Cross correlation residuals - expl. variable
res <- residuals(arima.fit)
res[is.na(res)] <- 0
ccf(y = res, x = x)
########

# Check fitted
autoplot(y, series = "Real")+
  forecast::autolayer(fitted(arima.fit), series = "Fitted")

