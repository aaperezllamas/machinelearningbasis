library(tidyverse)
library(GGally)
library(caret)
library(ggplot2)
library(ROCR)
library(pROC) #For function roc()
library(MLTools)


#1. Cargamos el conjunto de datos
mortgageClass = read.csv(file = "MortgageClass.csv", sep = ";", header = TRUE)


#2. Realizamos un análisis exploratorio de los datos 
#   (valores faltantes, atipicos y relaciones entre varibles)

str(mortgageClass)
glimpse(mortgageClass)
summary(mortgageClass)
mortgageClass$SingleFamily <- as.factor(mortgageClass$SingleFamily)
mortgageClass$Default <- as.factor(mortgageClass$Default)

levels(mortgageClass$Default)
levels(mortgageClass$Default) <- c("Not.paid", "Paid")
levels(mortgageClass$SingleFamily)
levels(mortgageClass$SingleFamily) <- c("Not.single", "Single")

any(is.na(mortgageClass)) #nos dice si hay Na en la tabla.
# complete.cases(mortgageClass) #nos localiza los NA en la tabla(Mediante un FALSE)
# which(complete.cases(mortgageClass) == FALSE)  #nos indica que fila tiene NA
# which(is.na(mortgageClass) == TRUE, arr.ind = TRUE) #nos indica que fila y columna tiene el NA
mortgageClass<-na.omit(mortgageClass)

#Valores atipicos
boxplot(mortgageClass)

#atipicos = boxplot(mortgageClass)
# mortgageClass = mortgageClass[-which(mortgageClass$balance %in% boxplot(mortgageClass$balance)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$balance_orig %in% boxplot(mortgageClass$balance_orig)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$time_to_maturity %in% boxplot(mortgageClass$time_to_maturity)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$LTV %in% boxplot(mortgageClass$LTV)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$LTV_orig %in% boxplot(mortgageClass$LTV_orig)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$interest_rate %in% boxplot(mortgageClass$interest_rate)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$Interest_Rate_orig %in% boxplot(mortgageClass$Interest_Rate_orig)$out),]
# mortgageClass = mortgageClass[-which(mortgageClass$FICO_orig %in% boxplot(mortgageClass$FICO_orig)$out),]
# boxplot(mortgageClass)


mortgageClass = mortgageClass[-which(mortgageClass$balance_orig == min(mortgageClass$balance_orig)),]
mortgageClass = mortgageClass[-which(mortgageClass$balance_orig > 1000000),]
mortgageClass = mortgageClass[-which(mortgageClass$balance > 1000000),]
# new cols
#mortgageClass$interestEvol = mortgageClass$interest_rate - mortgageClass$Interest_Rate_orig
#mortgageClass$LTVEvol = mortgageClass$LTV - mortgageClass$LTV_orig


## Eliminacion de variables
mortgageClass$age = NULL
#qda
#mortgageClass$balance_orig = NULL
#mortgageClass$Interest_Rate_orig = NULL

ggpairs(data = mortgageClass, mapping=ggplot2::aes(colour = Default))

#Dividimos los datos en training and test sets.
set.seed(150)
trainIndex <- createDataPartition(mortgageClass$Default, p = 0.8, list = FALSE, times = 1)
fTR <- mortgageClass[trainIndex,]
fTS <- mortgageClass[-trainIndex,]

#creamos matriz de control de cross validation
ctrl <- trainControl(method = "cv", number = 10, summaryFunction = defaultSummary, classProbs = TRUE)

#-------------------------------------------------------------------------------------------------
#---------------------------- LOGISTIC REGRESSION MODEL ----------------------------------------------
#-------------------------------------------------------------------------------------------------
## Train model -----------------------------------------------------------------------------------
set.seed(150) #For replication
#Train model using training data
LogReg.fit <- train(form = Default ~ ., #formula for specifying inputs and outputs.
                    data = fTR,               #Training dataset 
                    method = "glm",                   #Train logistic regression
                    preProcess = c("center","scale"), #Center an scale inputs
                    trControl = ctrl,                 #trainControl Object
                    metric = "Accuracy")            #summary metric used for selecting hyperparameters
LogReg.fit          #information about the resampling
summary(LogReg.fit) #detailed information about the fit of the final model
## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(LogReg.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(LogReg.fit, type="raw", newdata = fTR) # predict classes 

addmargins(prop.table(table(fTR_eval$Default, fTR_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(LogReg.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(LogReg.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)

#-------------------------------------------------------------------------------------------------
#--------------------------------- LDA -----------------------------------------------------------
#-------------------------------------------------------------------------------------------------

## Train LDA ------------------------------------------------------------------
set.seed(150) #For replication
#Train model using training data
lda.fit = train(form = Default ~ ., #formula for specifying inputs and outputs.
                data = fTR,               #Training dataset 
                method = "lda",  # Linear Discriminant Analysis
                preProcess = c("center","scale"), #Center and scale
                trControl = ctrl, 
                metric = "Accuracy")
lda.fit #information about the resampling results


## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(lda.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(lda.fit, type="raw", newdata = fTR) # predict classes 
addmargins(prop.table(table(fTR_eval$Default, fTR_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(lda.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(lda.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)


#-------------------------------------------------------------------------------------------------
#--------------------------------- QDA -----------------------------------------------------------
#-------------------------------------------------------------------------------------------------
## Train QDA -----------------------------------------------------------------------------------
set.seed(150) #For replication
#Train model using training data
qda.fit = train(form = Default ~ ., #formula for specifying inputs and outputs.
                data = fTR,               #Training dataset 
                method = "qda",   #Quadratic Discriminant Analysis
                preProcess = c("center","scale"), #Center and scale
                trControl = ctrl, 
                metric = "Accuracy")
qda.fit #information about the settings


## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(qda.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(qda.fit, type="raw", newdata = fTR) # predict classes 
addmargins(prop.table(table(fTR_eval$Default, fTR_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(qda.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(qda.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)

#-------------------------------------------------------------------------------------------------
#---------------------------- KNN MODEL  ----------------------------------------------
#-------------------------------------------------------------------------------------------------
set.seed(150) #For replication
#Train knn model model.
#Knn contains 1 tuning parameter k (number of neigbors). Three options:
#  - Train with a fixed parameter: tuneGrid = data.frame(k = 5),
#  - Try with a range of values specified in tuneGrid: tuneGrid = data.frame(k = seq(2,120,4)),
#  - Caret chooses 10 values: tuneLength = 10,
knn.fit = train(form = Default ~ ., #formula for specifying inputs and outputs.
                data = fTR,               #Training dataset 
                method = "knn",
                preProcess = c("center","scale"),
                #tuneGrid = data.frame(k = 5),
                tuneGrid = data.frame(k = seq(3,121,4)),
                #tuneLength = 10,
                trControl = ctrl, 
                metric = "Accuracy")
knn.fit #information about the settings
ggplot(knn.fit) #plot the summary metric as a function of the tuning parameter
knn.fit$finalModel #information about final model trained

## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(knn.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(knn.fit, type="raw", newdata = fTR) # predict classes 
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(knn.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(knn.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)
#-------------------------------------------------------------------------------------------------
#---------------------------- DECISION TREE ------------------------------------------------------
#-------------------------------------------------------------------------------------------------
library(rpart)
library(rpart.plot)
library(partykit)
set.seed(2019) #For replication
#Train decision tree
#rpart contains 1 tuning parameter cp (Complexity parameter). Three options:
#  - Train with a fixed parameter: tuneGrid = data.frame(cp = 0.1),
#  - Try with a range of values specified in tuneGrid: tuneGrid = data.frame(cp = seq(0,0.4,0.05))),
#  - Caret chooses 10 values: tuneLength = 10,

#NOTE: Formula method could be used, but it will automatically create dummy variables. 
# Decision trees can work with categorical variables as theey are. Then, x and y arguments are used, no se entrega en forma de formula
tree.fit <- train(x = fTR[,-which(names(fTR)=="Default")],  #Input variables. si usasemos form  = Y ~ automaticamente conviertes los inputs de factores a dummy en cambio de esta manera no  lo hace con el arbol de decison 
                  y = fTR$Default,   #Output variable, se le entrega de esta manera ya que el arbol de decision es mas inteligente y tiene en cuenta el tipo de diferencias en los factores.
                  method = "rpart",   #Decision tree with cp as tuning parameter
                  control = rpart.control(minsplit = 5,  # Minimum number of obs in node to keep cutting
                                          minbucket = 5), # Minimum number of obs in a terminal node
                  parms = list(split = "gini"),          # impuriry measure
                  #tuneGrid = data.frame(cp = 0.1), # TRY this: tuneGrid = data.frame(cp = 0.25),CP 0 maxima complejidad que no haya ningun error, OVERFITTING si tenemos algun outlier con CP = 0 se tendra que ajustar tambien
                  #tuneLength = 10,
                  tuneGrid = data.frame(cp = seq(0,0.1,0.005)), # secuencia de diferentes parametros para ver cual nos da mas accuracy
                  trControl = ctrl, 
                  metric = "Accuracy")
# tener ramificacion para cada conjunto de datos tiende a ser lo mas optimo
tree.fit #information about the resampling settings
ggplot(tree.fit) #plot the summary metric as a function of the tuning parameter
summary(tree.fit)  #information about the model trained
tree.fit$finalModel #Cuts performed and nodes. Also shows the number and percentage of cases in each node.
#Basic plot of the tree:
plot(tree.fit$finalModel, uniform = TRUE, margin = 0.1)
text(tree.fit$finalModel, use.n = TRUE, all = TRUE, cex = .8)
#Advanced plots
rpart.plot(tree.fit$finalModel, type = 2, fallen.leaves = FALSE, box.palette = "Oranges")
tree.fit.party <- as.party(tree.fit$finalModel)
plot(tree.fit.party)

#Measure for variable importance
varImp(tree.fit,scale = FALSE)
plot(varImp(tree.fit,scale = FALSE))

## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(tree.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(tree.fit, type="raw", newdata = fTR) # predict classes 
addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(tree.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(tree.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)

#-------------------------------------------------------------------------------------------------
#---------------------------- SVM RADIAL ------------------------------------------------------
#-------------------------------------------------------------------------------------------------
library(kernlab)
set.seed(150) #For replication
#Train model using training data
#Train radial  svm
#svm contains 2 tuning parameter C (Cost) and sigma. Three options:
#  - Train with a fixed parameter: tuneGrid = data.frame( sigma=100, C=1),
#  - Try with a range of values specified in tuneGrid: tuneGrid = expand.grid(C = seq(0.1,100,length.out = 8), sigma=seq(0.01,50,length.out = 4)),
#  - Caret chooses 10 values: tuneLength = 10,
svm.fit = train(form = Default ~ ., #formula for specifying inputs and outputs.
                data = fTR,   #Training dataset 
                method = "svmRadial",
                preProcess = c("center","scale"),
                tuneGrid = expand.grid(C = c(0.001,0.01,0.1,1,10,100,1000), sigma=c(0.0001,0.001,0.01,0.1,1,10,15)),#Combina valores de Cy Sigma
                #tuneGrid =  data.frame(sigma = 0.01, C = 0.1),  
                #tuneGrid = expand.grid(C = seq(0.1,1000,length.out = 8), sigma=seq(0.01,50,length.out = 4)),
                #tuneLength = 10,
                trControl = ctrl, 
                metric = "Accuracy")
svm.fit #information about the resampling settings
ggplot(svm.fit) + scale_x_log10()#curva azul aparentemente la mas bonita ya que mantiene una accuracy alta
svm.fit$finalModel #information about the model trained
## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(svm.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(svm.fit, type="raw", newdata = fTR) # predict classes 
addmargins(prop.table(table(fTR_eval$Default, fTR_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(svm.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(svm.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)

#-------------------------------------------------------------------------------------------------
#----------------------------------------MLP----- ------------------------------------------------
#-------------------------------------------------------------------------------------------------
library(NeuralNetTools) ##Useful tools for plotting and analyzing neural networks
library(nnet)
#Train MLP
#mlp contains 2 tuning parameters size and decay Three options:
#  - Train with a fixed parameter: tuneGrid = data.frame(size =5, decay = 0),
#  - Try with a range of values specified in tuneGrid: tuneGrid = expand.grid(size = seq(5,25,length.out = 5), decay=c(10^(-9),0.0001,0.001,0.01,0.1,1,10)),
#  - Caret chooses 10 values: tuneLength = 10,
set.seed(2019) #For replication
mlp.fit = train(form = Default ~ ., #formula for specifying inputs and outputs.
                data = fTR,   #Training dataset 
                method = "nnet",
                preProcess = c("center","scale"),
                maxit = 250,    # Maximum number of iterations
                #tuneGrid = data.frame(size =5, decay = 0),
                tuneGrid = expand.grid(size = seq(5,25,length.out = 5),#,size = seq(15,45,length.out = 7),
                                       decay=c(0.001,0.01,0.1,1,1.5,2)),
                trControl = ctrl, 
                metric = "Accuracy")

mlp.fit #information about the resampling settings
ggplot(mlp.fit)+scale_x_log10()
mlp.fit$finalModel #information about the model trained
#summary(mlp.fit$finalModel) #information about the network and weights
plotnet(mlp.fit$finalModel) #Plot the network

#Statistical sensitivity analysis
library(NeuralSens)
SensAnalysisMLP(mlp.fit) 



## Evaluate model --------------------------------------------------------------------------------
#Evaluate the model with training and test sets
#training
fTR_eval <- fTR
fTR_eval$prob <- predict(mlp.fit, type="prob", newdata = fTR) # predict probabilities
fTR_eval$pred <- predict(mlp.fit, type="raw", newdata = fTR) # predict classes 
addmargins(prop.table(table(fTR_eval$Default, fTR_eval$pred)))
length(which(fTR_eval$Default==fTR_eval$pred)==TRUE)/length(fTR_eval$Default)
#test
fTS_eval <- fTS
fTS_eval$prob <- predict(mlp.fit, type="prob", newdata = fTS) # predict probabilities
fTS_eval$pred <- predict(mlp.fit, type="raw", newdata = fTS) # predict classes 

addmargins(prop.table(table(fTS_eval$Default, fTS_eval$pred)))
length(which(fTS_eval$Default==fTS_eval$pred)==TRUE)/length(fTS_eval$Default)

#ROC curve
reducedRoc <- roc(response = fTS_eval$Default, fTS_eval$prob$Not.paid)
plot(reducedRoc, col="black")
auc(reducedRoc)

#histogram
ggplot(fTS_eval) + 
  geom_histogram(
    aes(
      x = fTS_eval$prob$Not.paid, #Estimated probabilities 
      fill=Default), #Fillwithrealoutput
    bins = 30) +
  facet_wrap(~Default)

#######Classification performance plots 
# Training
PlotClassPerformance(fTR_eval$Default,       #Real observations
                     fTR_eval$prob,  #predicted probabilities
                     selClass = "Paid") #Class to be analyzed
# test
PlotClassPerformance(fTS_eval$Default,       #Real observations
                     fTS_eval$prob,  #predicted probabilities
                     selClass = "Paid") #Class to be analyzed)





