library(ggfortify)

traffic <- read.table("TrafficData.dat",header = TRUE, sep = "", stringsAsFactors = FALSE)
fdata <- traffic[,5:28]

matplot(t(as.matrix(fdata[1:5,])),type="l",
        xlab = "Hours", ylab = "traffic", main = "Five traffic profiles")
# no neeed to scale

fdata.pca<-prcomp(fdata, center = TRUE, scale. = FALSE) # scale true cuando distintas unidades
summary(fdata.pca)

# con tres represento el 95% de la variabilidad de los datos, en este caso con 90% tenemos de sobra nos quedamos con dos
std_dev <- fdata.pca$sdev
pr_var <- std_dev^2 # varianza = desviación estandar al cuadrado
prop_varex <- pr_var/sum(pr_var)
barplot(prop_varex[1:5], xlab = "Principal Component",
        ylab = "Proportion of Variance Explained",
        names.arg=dimnames(fdata.pca$rotation)[[2]][1:5],
        ylim = c(0,1))
lines(cumsum(prop_varex[1:5]), col="blue", lwd = 3)
legend(x=3,y=0.8,legend = "Cumulative Proportion",col = "blue",lty = 1)
