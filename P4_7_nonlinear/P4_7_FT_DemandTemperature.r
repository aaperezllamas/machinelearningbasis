library(fpp2)
library(lmtest)
library(tseries) #contains adf.test function
library(TSA)
library(readxl)
library(Hmisc) #For Lag function
library(MLTools)

## Set working directory ---------------------------------------------------------------------------------------------


# Read dataset -------------------------------------------------------------------------------------------------
fdata <- read_excel("DAILY_DEMAND_TR.xlsx")
#Plot relation between temperature and demand
ggplot(fdata)+geom_point(aes(x=TEMP, y=DEM))
#We can see that there is a non-linear relation between them.

# variable para modelar los dias en WD con tecnica avanzada para que festivos no interfieran


##---------------------------------------------------------------
# GAM Modeling -------------------------------------------------
library(caret)
library(gam)
#Use resampling for measuring generalization error
#K-fold with 10 folds
ctrl_tune <- trainControl(method = "cv",                     
                          number = 10,
                          summaryFunction = defaultSummary,    #Performance summary for comparing models in hold-out samples.
                          returnResamp = "final",              #Return final information about resampling
                          savePredictions = TRUE)              #save predictions
set.seed(150) #For replication
gam.fit = train(form = DEM ~ WD + TEMP,
                data = fdata, 
                method = "gamSpline",
                tuneGrid = data.frame(df = seq(2,20,2)),# degrees of freedom indican si mas o menoos suave, cuando mas grande mas cuvada puede ser
                preProcess = c("center","scale"),
                trControl = ctrl_tune, 
                metric = "RMSE")
gam.fit #information about the resampling settings
ggplot(gam.fit)
plot(gam.fit$finalModel, se=TRUE ,col ="blue ")

#Check residuals
gam_pred = predict(gam.fit,  newdata = fdata)  
PlotModelDiagnosis(fdata, fdata$DEM, 
                   gam_pred,
                   together = TRUE)
#residuals correlated with output variable???
# curva frente a la salida indica que la salida esta correlada con salida, significa que hay una relacion temporal que no estamos modelando
ggtsdisplay(fdata$DEM-gam_pred)# demanda menos prediccion obtengo residuos
# observo que el residuo esta correlado temporarlemte y no es ruido blanco, lo que no es capaz de explicar se puede expresar con el tiempo
#residuals are not white noise!!! --> we need a time series model

##-------------------A PARTIR DE AQUI SE HACE EN CLASE--------------------------------------------
## Transfer function model --------------------------------------

#Create new time series splitting the temperatures
T.cold <- sapply(fdata$TEMP,min,  10)
T.hot <- sapply(fdata$TEMP,max, 17)
fdata <- cbind(fdata,T.cold,T.hot)

#Convert to time series object
fdata_ts <- ts(fdata)
autoplot(fdata_ts, facets = TRUE)
#Create time series 
y <- fdata_ts[,2]
x <- fdata_ts[,c( 3, 5, 6)]
autoplot(cbind(y,x), facets = TRUE)

#Scale
y.tr <- y/100
x.tr <- x/1
autoplot(cbind(y.tr,x.tr), facets = TRUE)


## Identification and fitting process -------------------------------------------------------------------------------------------------------

#### Fit initial FT model with large s
# This arima function belongs to the TSA package
TF.fit <- arima(y,
                order=c(1,1,0),
                seasonal = list(order=c(0,0,0),period=7),
                xtransf = x.tr,
                transfer = list(c(0,9),c(0,9),c(0,9)),
                include.mean = TRUE,
                method="ML")
summary(TF.fit) # summary of training errors and estimated coefficients
coeftest(TF.fit) # statistical significance of estimated coefficients
# Check regression error to see the need of differentiation
TF.RegressionError.plot(y,x,TF.fit,lag.max = 100)
#NOTE: If this regression error was not stationary in variance,boxcox should be applied to input and output series.
# WD b,r,s todo es 0 implica que no hay dinamica y un unico parametro arriba, significa que al dia siguiente no pasa nada relacionado con la i
# TCOLD opuestas por que las pendientes van opuestas b,r,s 0,1,0(s = 0 decae inmediatamente)
#
# Check numerator coefficients of explanatory variable
TF.Identification.plot(x,TF.fit)


#### Fit arima noise with selected  
x.tr.lag <- cbind(
  Lag(x.tr[,1],0),
  Lag(x.tr[,2],0),
  Lag(x.tr[,3],0)
)
x.tr.lag[is.na(x.tr.lag)] <- 0
arima.fit <- arima(y,
                   order=c(1,1,1),
                   seasonal = list(order=c(1,1,1),period=7),
                   xtransf = x.tr.lag,
                   transfer = list(c(0,0),c(1,0),c(1,0)),
                   include.mean = FALSE,
                   method="ML")
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
# Check residuals
CheckResiduals.ICAI(arima.fit,lag = 50)
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 50)

#Check Cross correlation residuals - expl. variables
res <- residuals(arima.fit)
res[is.na(res)] <- 0
ccf(y = res, x = x.tr[,1])
ccf(y = res, x = x.tr[,2])
ccf(y = res, x = x.tr[,3])




## Finished model identification
#Check fitted
autoplot(y.tr, series = "Real")+
  forecast::autolayer(fitted(arima.fit), series = "Fitted")

#Chech training errors of the model
accuracy(fitted(arima.fit),y.tr)


#################################################################################
## Forecast for new data with h = 7
#################################################################################

x.tv <- read_excel("DAILY_DEMAND_TV.xlsx")
#divide temperatures for validation
...


#Select input variables
x.tv <- ts(x.tv[,c( , , )])
#Obtain forecast for horizon = 7 using the trained parameters of the model
val.forecast_h7 <- TF.forecast(y.old = y.tr, #past values of the series
                              x.old = x.tr, #Past values of the explanatory variables
                              x.new = x.tv, #New values of the explanatory variables
                              model = arima.fit, #fitted transfer function model
                              h=7) #Forecast horizon

val.forecast_h7
