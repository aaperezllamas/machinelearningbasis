#################################################################################
##############   Forecasting: Decomposition Methods   ###########################
##############   ------------- solution -----------   ###########################
#################################################################################

library(fpp2)
library(ggplot2)

## Set working directory ---------------------------------------------------------------------------------------------


## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read.table("Unemployment.dat",header = TRUE, stringsAsFactors = FALSE)

#Convert to time series object
fdata_ts <- ts(fdata$TOTAL,start = 2009, frequency = 12)


#################################################################################
# Functions for plotting a time series
#################################################################################

#Plot time series
autoplot(fdata_ts) +
  ggtitle("Unemployment in Spain") +
  xlab("Year") + ylab("Number unemployed")

#If fdata_ts contains several time series, the plot can be faceted 
#autoplot(fdata_ts, facet = TRUE)

#################################################################################
# Functions for extracting a portion of a time series
#################################################################################

fdata_ts_cut <- window(fdata_ts, start=2013)
fdata_ts_cut <- window(fdata_ts, end=c(2013,12))
fdata_ts_cut <- window(fdata_ts, start = c(2012,5), end=c(2016,3))

#subset allows for more types of subsetting. It allows the use of indices to choose a subset.
fdata_ts_cut <- subset(fdata_ts, start=length(fdata_ts)-5)
#Head and tail are useful for extracting the first few or last few observations.
fdata_ts_cut <- tail(fdata_ts, 5)


#################################################################################
# Decomposition methods
#################################################################################


## Classical additive decomposition
fdata_ts_dec_add <- decompose(fdata_ts,type="additive")
autoplot(fdata_ts_dec_add) + xlab("Year") +
  ggtitle("Classical additive decomposition")

## Classical Multiplocative decomposition
fdata_ts_dec_mult <- decompose(fdata_ts,type="multiplicative")
autoplot(fdata_ts_dec_mult) + xlab("Year") +
  ggtitle("Classical multiplicative decomposition")


## SEATS
library(seasonal)
fdata_ts_dec_seas <- seas(fdata_ts)
autoplot(fdata_ts_dec_seas)+ xlab("Year") +
  ggtitle("SEATS decomposition")
# ----- An interactive app is avaliable ------
# library(seasonalview)
# view(fdata_ts_seas)
# --------------------------------------------

#Use seasonal(), trendcycle() and remainder() functions to extract the individual components.
#Use seasadj() to compute the seasonally adjusted time series.

#Compare seasonal components
autoplot(seasonal(fdata_ts_dec_add), series = "Additive")+
  forecast::autolayer(seasonal(fdata_ts_dec_mult), series = "Multiplicative") +
  forecast::autolayer(seasonal(fdata_ts_dec_seas),series = "SEATS")
autoplot(seasonal(fdata_ts_dec_mult), series = "Multiplicative") +
  forecast::autolayer(seasonal(fdata_ts_dec_seas),series = "SEATS")


#Compare seasonal adjustment components (i.e. substracting the seasonal component from the raw series)
autoplot(seasadj(fdata_ts_dec_add), series = "Additive")+
  forecast::autolayer(seasadj(fdata_ts_dec_mult), series = "Multiplicative") +
  forecast::autolayer(seasadj(fdata_ts_dec_seas),series = "SEATS")



 #Seasonal subseries plot
ggsubseriesplot(seasonal(fdata_ts_dec_add)) 
ggsubseriesplot(seasonal(fdata_ts_dec_mult)) 
ggsubseriesplot(seasonal(fdata_ts_dec_seas)) 

