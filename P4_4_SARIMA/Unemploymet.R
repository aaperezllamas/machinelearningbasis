#################################################################################
##############         Unenployment Forecasting      ############################
##############     ----------- solution ---------    ############################
#################################################################################

library(MLTools)
library(fpp2)
library(ggplot2)
library(TSA)
library(lmtest)  #contains coeftest function
library(tseries) #contains adf.test function
library(Hmisc) # for computing lagged variables

fdata <- read.table('Unemployment.dat', header = TRUE)

# Convert to time series object
y <- ts(fdata$TOTAL, start = 2009, frequency = 12)
autoplot(y)

## Identification and fitting frocess -------------------------------------------------------------------------------------------------------

# Box-Cox transformation
Lambda <- BoxCox.lambda.plot(y, 6)
# Lambda <- BoxCox.lambda(y) #other option
z <- BoxCox(y,-Lambda)
autoplot(z)

# Differentiation: if the ACF decreases very slowly -> needs differenciation
ggtsdisplay(z,lag.max = 25)

# If differencing is needed
Bz <- diff(z,differences = 1)
ggtsdisplay(Bz,lag.max = 100) #differences contains the order of differentiation

# Seasonal Differentiation
# If differencing is needed
B12Bz <- diff(Bz, lag = 6, differences = 1)#un orden de diferenciacion y el lag doce de los meses

# ACF and PACF of the time series -> identify significant lags and order
ggtsdisplay(B12Bz,lag.max = 100)

# Fit seasonal model with estimated order
arima.fit <- Arima(y,
                   order=c(1,1,1),
                   seasonal = list(order=c(1,1,3), period=6),
                   lambda = Lambda,
                   include.constant = FALSE)
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
autoplot(arima.fit) # root plot

# Check residuals
CheckResiduals.ICAI(arima.fit, bins = 24)
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 100)


y_est <- forecast(arima.fit, h=1)# prediccion a horizonte 12
autoplot(y_est)

write.csv(y_est, "predictionAlvaroBarbara.csv")








