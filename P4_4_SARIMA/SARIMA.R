library(MLTools)
library(fpp2)
library(ggplot2)
library(readxl)
library(lmtest)  #contains coeftest function
library(tseries) #contains adf.test function
# 1 comprobar si lo que me queda es ruido blanco
# 2 coeficientes son significativos
# Mas simple
fdata <- read_excel("SARIMA.xls")
y <- ts(fdata$NO2)#so2(1,1,2)(0,1,1)
autoplot(y)
# Box-Cox transformation
Lambda <- BoxCox.lambda.plot(y,7)
# Lambda <- BoxCox.lambda(y) #other option
z <- BoxCox(y,Lambda)
autoplot(z)

ggtsdisplay(z,lag.max = 100)

# If differencing is needed
Bz <- diff(z,differences = 7)
ggtsdisplay(Bz,lag.max = 100) #differences contains the order of differentiation

# Seasonal Differentiation
# If differencing is needed
B12Bz <- diff(Bz, lag = 7, differences = 1)
ggtsdisplay(B12Bz,lag.max = 100)

# ACF and PACF of the time series -> identify significant lags and order
ggtsdisplay(B12Bz,lag.max = 100)

# Fit seasonal model with estimated order
arima.fit <- Arima(y,
                   order=c(1,1,2),,#modelo reg 0 como he direnciado de las dos maneras pongo 1, completo con media movil de orden uno
                   seasonal = list(order=c(0,1,1),period=7),#012 arima orden dos estacional con esto y lo demas a 0,1,0 tiene buena pinta en la componente estacional
                   lambda = Lambda,
                   include.constant = FALSE)
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
autoplot(arima.fit) # root plot, sinedo p-valor mayor que 0.05 no puedo descartar que el ruido sea ruido blanco


# Check residuals
CheckResiduals.ICAI(arima.fit, bins = 100)#menos de 0.05 acepto hipotesis nula que dice que si es ruido blanco
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 100)


# Check fitted
autoplot(y.TR, series = "Real")+
  forecast::autolayer(arima.fit$fitted, series = "Fitted")
