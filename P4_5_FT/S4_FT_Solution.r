#################################################################################
##############      Lab 4.5:   Transfer Function     ############################
##############     ----------- solution ---------    ############################
#################################################################################

library(MLTools)
library(fpp2)
library(ggplot2)
library(TSA)
library(lmtest)  #contains coeftest function
library(tseries) #contains adf.test function
library(Hmisc) # for computing lagged variables

## Set working directory ---------------------------------------------------------------------------------------------

## Load dataset -------------------------------------------------------------------------------------------------------
fdata <- read.table("F50.dat",header = TRUE, sep = "")
colnames(fdata) <- c("Y","X")
# Convert to time series object
fdata_ts <- ts(fdata)
autoplot(fdata_ts, facets = TRUE)

# Create time series and scale values 
y <- fdata_ts[,1]
x <- fdata_ts[,2]
# Create time series and scale values 
y <- fdata_ts[,1]/10000
x <- fdata_ts[,2]/10000

ggtsdisplay(y, lag.max = 100)
ggtsdisplay(x)

## para F30
#### Fit initial FT model with large s
# This arima function belongs to the TSA package
TF.fit <- arima(y,
                order=c(1,1,0),#siemrpre que empezamos ponemos 1 en la componente autoregresiva para que le ayude a optimizar
                seasonal = list(order=c(0,1,0),period=24),
                xtransf = x,
                transfer = list(c(0,9)), #List with (r,s) orders, s numero de retardos
                include.mean = TRUE,
                method="ML")
summary(TF.fit) # summary of training errors and estimated coefficients
coeftest(TF.fit) 
TF.RegressionError.plot(y,x,TF.fit,lag.max = 100)
TF.Identification.plot(x,TF.fit)


#### Fit arima noise with selected
xlag = Lag(x,0)   # b
xlag[is.na(xlag)]=0
arima.fit <- arima(y,
                   order=c(0,1,2),
                   seasonal = list(order=c(0,1,1),period=24),
                   xtransf = xlag,
                   transfer = list(c(2,0)), #List with (r,s) orders
                   include.mean = FALSE,
                   method="ML")
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
# Check residuals
CheckResiduals.ICAI(arima.fit)# no puedo rechar que residuo ruido blanco, 0.237
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 100)
res <- residuals(arima.fit)
res[is.na(res)] <- 0
ccf(y = res, x = x)# no hay correlacion
## 


## Identification and fitting process F50 -------------------------------------------------------------------------------------------------------

#### Fit initial FT model with large s
# This arima function belongs to the TSA package
TF.fit <- arima(y,
                order=c(1,1,0),#siemrpre que empezamos ponemos 1 en la componente autoregresiva para que le ayude a optimizar
                seasonal = list(order=c(0,1,0),period=24),
                xtransf = x,
                transfer = list(c(0,9)), #List with (r,s) orders, s numero de retardos
                include.mean = TRUE,
                method="ML")
summary(TF.fit) # summary of training errors and estimated coefficients
coeftest(TF.fit) # statistical significance of estimated coefficients
# Check regression error to see the need of differentiation
TF.RegressionError.plot(y,x,TF.fit,lag.max = 100)# vemos como  ACF reduce linea en la variable temporar con disntidos malores
#NOTE: If this regression error is not stationary in variance,boxcox should be applied to input and output series.
#veo picos en la funcion de autocorrelacion significa que los valores esperados a una hota no son los musmo que a otra por lo tanto no es estacionaria
# Check numerator coefficients of explanatory variable
TF.Identification.plot(x,TF.fit)


#### Fit arima noise with selected
xlag = Lag(x,0)   # b
xlag[is.na(xlag)]=0
arima.fit <- arima(y,
                   order=c(0,1,2),
                   seasonal = list(order=c(1,1,0),period=24),
                   xtransf = xlag,
                   transfer = list(c(0,0)), #List with (r,s) orders
                   include.mean = FALSE,
                   method="ML")
summary(arima.fit) # summary of training errors and estimated coefficients
coeftest(arima.fit) # statistical significance of estimated coefficients
# Check residuals
CheckResiduals.ICAI(arima.fit)# no puedo rechar que residuo ruido blanco, 0.237
# If residuals are not white noise, change order of ARMA
ggtsdisplay(residuals(arima.fit),lag.max = 50)


### Cross correlation residuals - expl. variable
res <- residuals(arima.fit)
res[is.na(res)] <- 0
ccf(y = res, x = x)# no hay correlacion
########

# Check fitted
autoplot(y, series = "Real")+
  forecast::autolayer(fitted(arima.fit), series = "Fitted")

